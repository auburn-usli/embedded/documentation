% Control Theory sections


% Control Theory section intro
\section{Control Theory}
This section will try to cover control theory as simply as possible. Control
theory can be complicated, but it is necessary to have some knowledge and
background before trying to design a controller for a system. This section will
also introduce ways of expressing systems and the benefits for each one. It will
also discuss coordinate frames and their importance, and how to express vectors
from one coordinate frame in another. UPDATE 

The simplest way to implement an active
controller is by using a \emph{feedback controller}. A feedback controller takes
the output of a system or process and uses it to calculate an input to the
system. In our case, the output of our system is the apogee of the rocket, and
the input is the position of the drag mechanism. 

\subsection{PID Controllers}
One of the most basic feedback controllers is a PID controller, or Proportional,
Integral, Derivative controller. In Figure \ref{fig:PID Controller} you will see
the output of the \emph{Plant/Process} block is fed into a summation block (the
$\Sigma$ block). There you will see an $r(t)$ input into the summation block as
well. $r(t)$ is a \emph{reference} or \emph{setpoint}, this is the ``target'' of
the PID controller. $e(t)$ is then the \emph{error}, the difference between the
setpoint and the output of the system. From there, the error is used to
calculate the proportional, integral, and derivative terms. These are summed
together to create a new input to the system.

\begin{figure}[H]
    \label{fig:PID Controller}
    \centering
    \includegraphics[width=0.75\textwidth]{control/PID_en.png}
    \caption{Basic PID Controller}
\end{figure}


Below in Table \ref{tab:PID Terms} is a brief description of what each term
essentially does. Depending on your situation, the derivative term may not be
necessary, but it is useful if you have a system that likes to oscillate. If you
have a system that does not change quickly then a PI controller will suffice.
\medskip

\begin{table}[H]
    \basictable{lX}{
        Term & Description \\ 
    }{
        Proportional & 
        The error itself, which also drives the other two terms. \\
        
        Integral & 
        The error accumulation over time. Will compensate if the
        controller is lagging below or above the target and will increase its
        value until the controller reaches the setpoint. This is helpful for
        reducing error as time goes on, but can suffer from windup.\\
        
        Derivative & 
        The rate of change of the error. The derivative term acts
        as a damper, increasing its value if the error is changing very quickly.
        It is similar to driving a car and coming up to a stop sign quickly,
        where you would have to stomp on the brakes to avoid overshooting the
        sign.  \\
    }
    \caption{PID Controller Terms}
    \label{tab:PID Terms}

\end{table}


There are other ways of controlling a system, more complicated systems that have
more than one input and output would be too much for a PID controller, in that
scenario a full state feedback controller would be more appropriate. However,
since we have a single input and single output scenario, the PID controller will
do just fine.

\subsection{Vehicle Apogee} \label{vehicle_apogee}
Once we have created a PID controller, we will need a way of calculating the
output of the system in order to feed an input into the controller. We will need
to derive an equation to calculate the apogee of the rocket given its current
state. Below is a derivation of that equation. The equations we will start off
with can be found at 
\href{https://www.grc.nasa.gov/www/k-12/airplane/flteqs.html}{\emph{this page}}. 
For an object flying in an atmosphere, it is equation of motion is, 
$$
    a = -g - \frac{C_D S \rho V^2}{2m}
$$

where $C_D$ is the drag coefficient, $S$ is the reference area, $\rho$ is the
air density, $V$ is the vertical velocity, and $m$ is its mass. Also, the apogee
of an object is described by,

$$
    x_{ap} = \frac{V_t^2}{2g}\ln{\frac{V_0^2 + V_t^2}{V_t^2}}
$$

where $V_t$ is the terminal velocity, $V_t^2$ being described by,
$$
    V_t^2 = \frac{2mg}{C_DS\rho}
$$

We know some of the variables above beforehand, or can measure some of them
using sensors. However, we do not know $C_D$ for sure, so it would be nice to
eliminate it. If we take the equation of motion and solve for $C_D$, we get,

$$
    C_D = -\frac{2m}{\rho S V^2}(a + g)
$$ 

which, taking the new equation for $C_D$ and plugging them into the equation for
$V_t$, gives us this new equation for $V_t$ without $C_D$,

$$
    V_t^2 = \frac{gV^2}{-(a+g)}
$$

We also conveniently got rid of $\rho$. If we take this new $V_t$ equation and
substitute it into our $x_{ap}$ equation, and do some simplification, we get the
following,

$$
    x_{ap} = -\frac{V^2}{2(a+g)}\ln(-\frac{a}{g})
$$

The negative signs being a product of how we defined which way was positive and
negative in the equation of motion. To make it more robust, we can get rid of
the negative signs by taking the absolute value, and by adding our current
altitude, we finally get,

\begin{equation}
    x_{ap} = \frac{V^2}{2|a+g|}\ln(|\frac{a}{g}|) + x_0
\end{equation} \listofequations{Vehicle Apogee}

This is the equation we can use to calculate the apogee of the vehicle, which we
can use for our PI controller. The nice thing about this equation is it only
depends on the current state of the vehicle; it does not need to measure the
density of the air or calculate the drag coefficient. We only need an
accelerometer and altimeter, by which we can find the vertical acceleration,
velocity, and the altitude.

\subsection{Coordinate Frames and Transformations} \label{Coordinate Frames and Transformations}
Now that we have an equation for calculating the apogee, we need some sensors
for collecting the data we need. But, there is another small problem. The
equation we derived in \ref{vehicle_apogee} is a one-dimensional problem, and
assumes we are aligned with the gravity vector. An accelerometer will also give
data in the direction it is pointing, and since it is attached to the rocket, it
will always point towards where the rocket is, not straight up. The rocket's
frame of reference is the \emph{body} frame, and the ``world'' reference frame is
called the \emph{inertial} reference frame, and there are ways to express a
vector in one of these reference frame within the other, which this section
explores.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.375\textwidth]{control/coord_rotations.jpg}
    \caption{Separate Coordinate Frames}
    \label{fig:Coordinate Frame Rotation}
\end{figure}

\subsubsection{Transformation Matrix}
To express a vector from one frame in another, you would use something called a
transformation matrix. All you would need to do to express your current vector
in the other frame is multiply that vector by this transformation matrix. Given
a vector $\vec{A_B}$ and a transformation matrix $T_{B\rightarrow I}$ we can get
a vector in the other frame like so,

\begin{equation} \label{transform_eq}
    \vec{A_I} = T_{B\rightarrow I}\vec{A_B}
\end{equation} \listofequations{Frame Transform}

For example, let us assume we have an accelerometer sitting at some weird angle
and we want to know what the current acceleration is in the z direction in the
inertial frame. In this example, it just so happens to be gravity. In practice,
the transformation matrix and vectors might look something like this,

\begin{align*}
    \begin{bmatrix}
        0 \\
        0 \\
        32.174
    \end{bmatrix}_I
    =
    \begin{bmatrix}
        0.423 & 0.523 & 0.124 \\
        0.965 & 0.634 & 0.145 \\
        0.745 & 0.223 & 0.974 \\
    \end{bmatrix}_{B\rightarrow I}
    \begin{bmatrix}
        12.534 \\
        -2.643 \\
        18.174
    \end{bmatrix}_B
\end{align*}

These numbers are made up, so the math will not work out properly in this case,
but this is what it is supposed to \emph{look} like. Luckily, the \emph{IMUs}
you can find on the market can give you what you need to calculate a
transformation matrix for you. This is where \emph{quaternions} come in.

% \pagebreak

\subsubsection{Quaternions}
There is another way of calculating a transformation matrix using Euler angles.
While it is more intuitive, there are some good reasons to use quaternions
instead. This is why I am starting off with quaternions and not Euler angles. If
you are interested in learning more about the Euler angle method then you can
skip this section. But if you just want to know how quaternions are used to
calculate transformation matrices then just read this section.

Quaternions are similar to vectors, but they consist of one real number and
three imaginary ones. More generically, they are 3D versions of complex numbers.
For our purposes, a \emph{quaternion} represents a rotation about \emph{one}
axis. A quaternion looks like this,

\begin{equation}
    \vec{q} =
    \begin{bmatrix}
        r \\
        i \\
        j \\
        k \\
    \end{bmatrix}
\end{equation} \listofequations{Quaternion}

An \emph{IMU} will be able to calculate and provide these values to you, so you
do not need to worry about how to find these values. So now that you have a
\emph{quaternion} with the values \emph{r, i, j, k}, you can create a
transformation matrix by using the following method,

\begin{equation}
    T_{B\rightarrow I} =
    \begin{bmatrix}
        1 - 2(j^2 + k^2)  &  2(ij - kr)        &  2(ik + jr)      \\
        2(ij + kr)        &  1 - 2(i^2 + k^2)  &  2(jk - ir)      \\
        2(ik - jr)        &  2(jk + ir)        &  1 - 2(i^2 + j^2) \\
    \end{bmatrix}
\end{equation} \listofequations{Quaternion Derived Transformation Matrix}

That is it, that is all you need to do to calculate a transformation matrix. If
you want to read up on it more you can go
\href{https://en.wikipedia.org/wiki/Quaternions_and_spatial_rotation#Quaternion-derived_rotation_matrix}{\emph{here}}
for a more detailed explanation. You will notice I left out the $s$ term from
the matrix above, because the \emph{IMU} we use returns unit \emph{quaternions},
so $s=1$.

\subsubsection{Why not Euler Angles?}
If you have browsed some \emph{IMU} documentation, like the \emph{BNO055} we
use, you will know that you can also get the pitch, yaw, and roll as angles.
Using them, you could also get a transformation matrix in a similar way. Given
pitch, yaw, and roll as $\theta$, $\phi$, and $\psi$, you can find a
transformation matrix like the one below. $s()$ is short for $\sin()$ and $c()$
is short for $\cos()$

\begin{equation}
    T_{B\rightarrow I} =
    \begin{bmatrix}
        c(\theta)c(\phi) & c(\theta)s(\phi) & -s(\theta) \\

        s(\phi)s(\theta)c(\phi) - s(\phi)c(\phi) &
        c(\phi)c(\phi) + s(\phi)s(\psi)s(\theta) &
        s(\phi) c(\theta) \\

        s(\theta)c(\psi)c(\phi) + s(\phi)s(\psi) &
        c(\phi)s(\theta) s(\psi) - c(\psi)s(\phi) &
        c(\phi) c(\theta) \\
    \end{bmatrix}
\end{equation} \listofequations{Euler Angle Derived Transformation Matrix}

\normalsize
As you can see from all of the \emph{sins} and \emph{cosines}, this method is a
bit more computationally expensive, but not much worse than the
\emph{quaternion} method. However, the biggest problem with this method is
\emph{gimbal lock}. Gimbal lock is a loss of one degree of freedom when two axes
of a three axis gimbal are driven into a ``parallel'' configuration. In other
words, when two axes are rotating about the same axis. That might not make much
sense, but google gimbal lock gifs and it will. This is the reason why
\emph{quaternions} are more ``stable''.

\subsection{State Space}
The idea behind state space is to take a differential equation, normally an
order higher than one, and turn into a system of first order differential
equations in matrix form. The general form of a state space model is as follows,

\begin{equation}
    \begin{aligned}
    \vec{\dot{x}} = A\vec{x} + B\vec{u} \\
    y = C\vec{x} + D\vec{u}
    \end{aligned}
\end{equation}

$A$ is the matrix describing the dynamics of the system, $B$ is a matrix
describing the input to the system, and $\vec{x}$ is the state of the system.
The dynamics of the system are then $\vec{\dot{x}}$, being a combination of the
state and input. The $C$ matrix is used to take the state of the system and
return an output $y$, and $D$ is a feedthrough from the input of the system to
the output. $D$ is most often zero; it is only used if the output of the system
depends directly on the input in some way, which normally it does not. In block
diagram form, a state space model would look like this,

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{control/Typical_State_Space_model.png}
    \caption{State Space Block Diagram}
    \label{}
\end{figure}

\subsection{Kalman Filter} \label{Kalman Filter}
Since sensors are not perfect, there is always a certain amount of noise in
readings. A Kalman filter is a good choice for filtering the noise out of sensor
data. It is more of an estimator than a filter, and it needs to know the
dynamics of the system to work properly. However, the benefit of this is low
computational overhead and very accurate results.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{control/kalman_f.png}
    \caption{Kalman Filter Algorithm}
    \label{fig:kf_algo}
\end{figure}